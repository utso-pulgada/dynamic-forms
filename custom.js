$(function(){

	$('.btn-tool').click(function(){
		$('.btn-tool.active').removeClass('active');
		$(this).addClass('active');
		$('#elements-options-btn').click();

		if( $.trim( $(this).text() ) == 'select' ){
			$('.modal-dialog .radio-ck').hide();
			$('.modal-dialog .item-options-section').show();
		}else{
			if( $.trim( $(this).text() ) == 'checkbox' || $.trim( $(this).text() ) == 'radio' ){
				$('.modal-dialog .radio-ck').show();
			}else{
				$('.modal-dialog .radio-ck').hide();
			}
			$('.modal-dialog .item-options-section').hide();
		}

	});


	$(document).on('click','.modal-dialog .btn-submit',function(){
		// -- append element with label
		
		if( $.trim( $('.btn-tool.active').text() ) == 'button' || $.trim( $('.btn-tool.active').text() ) == 'submit' ){
			$('#template-form .contents').append('<div data-toolid="'+$('.btn-tool.active').attr('data-toolid')+'" class="form-item-wrapper active" style="margin-top:16px"><a href="#" class="delete-element"><i class="fas fa-times-circle"></i></a>'+$('#tool-'+$('.btn-tool.active').attr('data-toolid')).html()+'</div>');
		}else if( $.trim( $('.btn-tool.active').text() ) == 'radio' || $.trim( $('.btn-tool.active').text() ) == 'checkbox' ){
			$('#template-form .contents').append('<div data-toolid="'+$('.btn-tool.active').attr('data-toolid')+'" class="form-item-wrapper active"><a href="#" class="delete-element"><i class="fas fa-times-circle"></i></a>'+$('#tool-'+$('.btn-tool.active').attr('data-toolid')).html()+'<label style="margin-left:8px;">'+$('.modal-dialog input[data-id="element-label"]').val()+'</label></div>');

		}else{
			$('#template-form .contents').append('<div data-toolid="'+$('.btn-tool.active').attr('data-toolid')+'" class="form-item-wrapper active"><a href="#" class="delete-element"><i class="fas fa-times-circle"></i></a><label style="margin-bottom:2px;">'+$('.modal-dialog input[data-id="element-label"]').val()+'</label>'+$('#tool-'+$('.btn-tool.active').attr('data-toolid')).html()+'</div>');
		}

		if( $.trim( $('.btn-tool.active').text() ) == 'checkbox' ){
			if( $('.modal-dialog .radio-ck input[data-id="element-checked"]').is(':checked') ){
				$('.form-item-wrapper.active input[type="checkbox"]').prop('checked',true);
			}else{
				$('.form-item-wrapper.active input[type="checkbox"]').prop('checked',false);
			}
		}


		if( $.trim( $('.btn-tool.active').text() ) == 'radio' ){
			if( $('.modal-dialog .radio-ck input[data-id="element-checked"]').is(':checked') ){
				$('.form-item-wrapper.active input[type="radio"]').prop('checked',true);
			}else{
				$('.form-item-wrapper.active input[type="radio"]').prop('checked',false);
			}
		}


		if( $.trim( $('.btn-tool.active').text() ) == 'select' ){
			var options = '';

			$('.modal-dialog .item-option').each(function(){
				if( $.trim( $(this).find('input[data-id="element-option-label"]').val() ) != '' ){
					var ck = '';
					if( $(this).find('input[data-id="element-option-checked"]').is(':checked') ){
						ck=' selected';
					}
					options+='<option value="'+$(this).find('input[data-id="element-option-value"]').val()+'"'+ck+'>'+$(this).find('input[data-id="element-option-label"]').val()+'</option>';
				}
			});

			$('.form-item-wrapper.active select').html(options);
		}
		
		// -- attach attribute
		if( $('.form-item-wrapper.active input').length > 0 ){
			$('.form-item-wrapper.active input').addClass('element').attr('name',$('.modal-dialog input[data-id="element-name"]').val());
		}

		if( $('.form-item-wrapper.active select').length > 0 ){
			$('.form-item-wrapper.active select').addClass('element').attr('name',$('.modal-dialog input[data-id="element-name"]').val())
		}
		if( $('.form-item-wrapper.active textarea').length > 0 ){
			$('.form-item-wrapper.active textarea').addClass('element').attr('name',$('.modal-dialog input[data-id="element-name"]').val())
		}

		if( $('.form-item-wrapper.active button').length > 0 ){
			$('.form-item-wrapper.active button').addClass('element').text($('.modal-dialog input[data-id="element-label"]').val())
		}

		if( $('.modal-dialog input[data-id="element-required"]').is(':checked') ){
			$('.form-item-wrapper.active .element').prop('required',true);
		}else{
			$('.form-item-wrapper.active .element').prop('required',false);
		}
		if( $('.modal-dialog input[data-id="element-readonly"]').is(':checked') ){
			$('.form-item-wrapper.active .element').prop('readonly',true);
		}else{
			$('.form-item-wrapper.active .element').prop('readonly',false);
		}
		if( $('.modal-dialog input[data-id="element-disabled"]').is(':checked') ){
			$('.form-item-wrapper.active .element').prop('disabled',true);

		}else{
			$('.form-item-wrapper.active .element').prop('disabled',false);
		}

		$('.form-item-wrapper.active select,#template-form .form-item-wrapper.active input,#template-form .form-item-wrapper.active textarea').val($('.modal-dialog input[data-id="element-value"]').val());

		$('.modal-dialog .item-option-wrapper').html('') // -- clear the options wrapper
		$('.modal-dialog .close').trigger('click'); // -- close modal
	});

	$(document).on('click','.modal-dialog .add-option',function(){
		$('.modal-dialog .item-option-wrapper').append(`<div class="item-option">
		        		<div class="row">
		        			<div class="col-12">
					        	<input type="checkbox" data-id="element-option-checked" style="margin-bottom:8px">
					        	<label>Default</label>
					        </div>
		        			<div class="col-12">
					        	<input type="text" data-id="element-option-label" placeholder="Label" class="form-control" style="margin-bottom:8px">
					        </div>
					        <div class="col-12">
					        	<input type="text" data-id="element-option-value" placeholder="value" class="form-control" style="margin-bottom:8px">
					        </div>
					        <div class="col-12">
								<button class="btn btn-sm btn-primary btn-remove-option">Remove</button>
					        </div>
		        		</div>
	        		</div>`);
	});

	$(document).on('click','.modal-dialog .btn-remove-option',function(){
		$(this).closest('.item-option').remove();
	});

	$(document).on('click','.delete-element',function(){
		$(this).closest('.form-item-wrapper').remove();
	});

	$('#elements-options').on('show.bs.modal', function (event) {
		$('.form-item-wrapper.active').removeClass('active'); // -- clear active class form modal item wrapper
		$('.modal-dialog input').val('') // -- clear modal item value
		$('.modal-dialog input[type="checkbox"]').prop('checked',false);
	});

	$('#create-form').click(function(){

		$('#template-form .contents .delete-element').remove();
		$('#template-form .contents input,#template-form .contents textarea,#template-form .contents select').each(function(){
			$(this).attr('value',$(this).val());
		});

		var data = {
			template_id : $('#template-form').attr('data-templateid'),
			url : $('#template-form .form-identity input[name="url"]').val(),
			title : $('#template-form .form-identity input[name="title"]').val(),
			description : $('#template-form .form-identity textarea[name="description"]').val(),
			contents : $('#template-form .contents').html()
		}

		$.post('core.php',data,function(res){

			alert('Form '+data.template_id+' has been created successfully');

			if( res.success ){
				$('#template-form .contents').html('');
				$('#template-form input,#template-form textarea').val('');

				$('#templates-table tbody').append('<tr><td>'+res.new.hash+'</td><td>'+res.new.title+'</td><td>'+res.new.url+'</td><td>'+res.new.description+'</td><td style="text-align:center"><a href="index.php?req=template&id='+res.new.hash+'" class="btn btn-primary btn-sm" target="_BLANK">View</a></td></tr>');
				$('#gototemplates-table').trigger('click');
			}

		}).fail(function(err){
			console.log(err);
			alert('Error occured while creating form');
		});

	})

});