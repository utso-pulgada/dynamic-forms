<?php
	
	require_once(__DIR__.'/core.php');

	$core = new core;

	$id = $core->generateTemplateID();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Dynamic Forms</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="main">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-8 offset-sm-2 col-md-8 offset-md-2">
						<div class="form-wrapper">
							<?php if( isset( $_GET['req'] ) && $_GET['req'] == 'template' ){ ?>
							<h3 style="margin-bottom:8px"><?php echo $core->getTemplate($_GET['id'])->title; ?></h3>
							<p style="margin-bottom:24px"><?php echo $core->getTemplate($_GET['id'])->description; ?></p>
							<form action="<?php echo urldecode($core->getTemplate($_GET['id'])->url); ?>" method="post">
							<?php echo html_entity_decode($core->getTemplate($_GET['id'])->contents); ?>
							</form>
							<?php }else{ ?>
							<p style="opacity:.7">Click to add element</p>
							
								<div class="tools">
								<?php $core->tools(); ?>
								</div>
							<h4 style="margin-top:32px;"><?php echo $id; ?></h4>
							<div data-templateid="<?php echo $id; ?>" id="template-form" method="post" action="core.php" class="form">
								<div class="form-identity">
									<label>Form url</label>
									<input type="text" class="form-control" name="url" style="margin-bottom:8px;">
									<label>Name</label>
									<input type="text" class="form-control" name="title" style="margin-bottom:8px;">
									<label>Description</label>
									<textarea class="form-control" name="description"></textarea>
								</div>
								<hr style="margin-top:16px;margin-bottom:16px">
								<label style="margin-bottom: 8px">Elements</label>
								<div class="contents"></div>
								<button class="btn btn-primary btn-block" id="create-form">Create Form</button>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php if( !isset( $_GET['req'] ) ){ ?>
				<div class="row" style="margin-top:32px;">
					<div class="col-12 col-sm-10 offset-sm-1 col-md-10 offset-md-1">
						<div class="form-wrapper">
							<h4 style="margin-bottom:16px">Form Templates</h4>
							<div class="table-responsive">
								<table class="table table-bordered" id="templates-table">
									<thead>
										<tr>
											<th>
											Hash
											</th>
											<th>
											Title
											</th>
											<th>
											Url
											</th>
											<th>
											Description
											</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach( $core->getTemplates() as $t ){ ?>
										<tr>
											<td>
												<?php echo $t->hash; ?>
											</td>
											<td>
												<?php echo $t->title; ?>
											</td>
											<td>
												<?php echo urldecode($t->url); ?>
											</td>
											<td>
												<?php echo $t->description; ?>
											</td>
											<td style="text-align:center">
												<a href="index.php?req=template&id=<?php echo $t->hash; ?>" class="btn btn-primary btn-sm" target="_BLANK">View</a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
								<a href="#" id="gototemplates-table" style="display:none;">gototemplates-table</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</section>
	</div>
	<button type="button" style="display:none;" id="elements-options-btn" class="btn btn-primary" data-toggle="modal" data-target="#elements-options">
    Open modal
  </button>
	<!-- The Modal -->
  <div class="modal fade" id="elements-options">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Element options</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="item-default row" style="margin-bottom:16px">
	          	<div class="col-12">
					<label>Default</label>
          		</div>
	          	<div class="col-12" style="margin-bottom:8px">
	          		<input type="text" data-id="element-label" placeholder="label" class="form-control">
          		</div>
          		<div class="col-12" style="margin-bottom:8px">
		        	<input type="text" data-id="element-name" placeholder="name" class="form-control">
		        </div>
		        <div class="col-12" style="margin-bottom:8px">
		        	<input type="text" data-id="element-value" placeholder="value" class="form-control">
		        </div>
		       	<div class="col-6 radio-ck" style="margin-bottom:8px">
		        	<input type="checkbox" data-id="element-checked">
		        	<label>Default</label>
		        </div>
		        <div class="col-6 ck-required" style="margin-bottom:8px">
		        	<input type="checkbox" data-id="element-required">
		        	<label>Required</label>
		        </div>
		        <div class="col-6 ck-readonly" style="margin-bottom:8px">
		        	<input type="checkbox" data-id="element-readonly">
		        	<label>Readonly</label>
		        </div>
		        <div class="col-6 ck-disabled" style="margin-bottom:8px">
		        	<input type="checkbox" data-id="element-disabled">
		        	<label>Disabled</label>
		        </div>
          </div>
          <div class="item-options-section row" style="display:none;margin-bottom:16px">
          		<div class="col-12">
					<label>Options</label>
					<span class="add-option" style="font-size:11px;text-decoration:underline;margin-left:16px;cursor:pointer">Add option</span>
          		</div>
	        	<div class="col-12 item-option-wrapper">
	        	</div>
	        </div>
          <div class="row">
          	<div class="col-12">
				<button class="btn btn-sm btn-primary btn-block btn-submit">Save</button>
	        </div>
          </div>    
        </div>
        
      </div>
    </div>
  </div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="custom.js"></script>
</body>
</html>