-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 11, 2019 at 03:05 AM
-- Server version: 5.7.24
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `dynamic_forms`
--

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(40) NOT NULL,
  `html` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `type`, `html`, `created_at`, `updated_at`) VALUES
(1, 'text', '&lt;input type=&quot;text&quot; class=&quot;form-control&quot;&gt;', '2019-12-10 03:56:43', '2019-12-10 06:05:27'),
(2, 'date', '&lt;input type=&quot;date&quot; class=&quot;form-control&quot;&gt;', '2019-12-10 03:56:43', '2019-12-10 06:05:38'),
(3, 'number', '&lt;input type=&quot;number&quot; class=&quot;form-control&quot;&gt;', '2019-12-10 03:58:13', '2019-12-10 06:05:45'),
(4, 'radio', '&lt;input type=&quot;radio&quot;&gt;', '2019-12-10 03:58:13', '2019-12-10 06:06:11'),
(5, 'checkbox', '&lt;input type=&quot;checkbox&quot;&gt;', '2019-12-10 03:59:37', NULL),
(6, 'select', '&lt;select class=&quot;form-control&quot;&gt;&lt;/select&gt;', '2019-12-10 03:59:37', '2019-12-10 06:07:46'),
(7, 'textarea', '&lt;textarea class=&quot;form-control&quot;&gt;&lt;/textarea&gt;', '2019-12-10 04:32:15', '2019-12-10 06:07:57'),
(8, 'button', '&lt;button class=&quot;btn btn-primary&quot;&gt;&lt;/button&gt;', '2019-12-10 05:52:34', '2019-12-10 06:27:17'),
(9, 'submit', '&lt;input type=&quot;submit&quot; class=&quot;btn btn-primary&quot;&gt;', '2019-12-10 05:52:34', '2019-12-10 06:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `contents` longtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
COMMIT;
