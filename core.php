<?php

	require_once(__DIR__.'/vendor/autoload.php');



	class core{

		public $db;

		function __construct(){
			$this->db = new MysqliDb ('localhost', 'root', '', 'dynamic_forms');
		}
		public function getElement($type){

			$this->db->where('type',$type)->get('elements');

		}

		public function tools(){

			$tools = $this->db->ObjectBuilder()->get('elements');

			foreach( $tools as $t ){

				echo '<button data-toolid="'.$t->id.'" style="margin:2px;" class="btn-tool btn btn-small btn-sm btn-primary">'.$t->type.'</button>';

				echo '<div style="display:none" class="tool-wrapper" id="tool-'.$t->id.'">'.html_entity_decode($t->html).'</div>';

			}
		}

		public function generateTemplateID(){

			return 'template-'.uniqid().'-'.count( $this->db->ObjectBuilder()->get('templates') );

		}

		public function createForm(){

			header('Content-type:application/json');

			$data = [
				'url' => urlencode( $_POST['url'] ),
				'title' => $_POST['title'],
				'hash' => $_POST['template_id'],
				'description' => $_POST['description'],
				'contents' => htmlentities( $_POST['contents'] ),
			];


			
			if( $this->db->insert('templates',$data) ){
				$data['contents'] = '';
				echo json_encode([ 'success' => true, 'msg' => 'Successfully created a form id: '.$data['hash'], 'new' => $data ]);
			}else{
				echo json_encode([ 'success' => false, 'msg' => 'Error creating form id: '.$data['hash'] ]);
			}

		}

		public function getTemplates(){

			return $this->db->ObjectBuilder()->get('templates');

		}

		public function getTemplate($id){
			return $this->db->ObjectBuilder()->where('hash',$id)->getOne('templates');
		}

		public function deleteTemplate($id){

			header('Content-type:application/json');

			$this->db->where('hash',$id)->delete('templates');

			echo json_encode([ 'success' => true, 'msg' => 'Successfully deleted templates' ]);
		}
	}


	if( isset( $_GET['req']) ){

		switch( $_GET['req'] ){

			case 'element':
			var_dump( ( new core )->getElement( $_GET['type'] ) );
			break;
		}

	}

	if( isset( $_POST['template_id'] ) ){

		// var_dump($_POST);

		( new core )->createForm();

	}


	if( isset( $_POST['delete'] ) ){

		// var_dump($_POST);

		( new core )->deleteTemplate($_POST['id']);

	}
